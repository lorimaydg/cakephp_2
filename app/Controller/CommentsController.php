
<?php

    App::import('Controller', 'Posts');
    App::import('Controller', 'Users');

    class CommentsController extends AppController {

        public function view ($uid = null, $pid = null) {

            $comment = $this->Comment->find('all', array(
            	'contain' => array('User','Post'), 
                'conditions' => array(
                    'Post.id' => "$pid"
                )
            ));
            $this->set('comments', $comment);

            $PostOwner = new PostsController;
            $PostOwner->PostOwner($uid);

            $this->sidebar();
        }

        public function add ($uid = null, $pid = null) {

        	if ($this->request->is('post')) {
                $this->request->data['Comment']['user_id'] = $this->Auth->user('id');
                $this->request->data['Comment']['post_id'] = $pid;
                if ($this->Comment->save($this->request->data)) {
                    return $this->redirect(array('action' => 'view', $uid, $pid));
                }
            }
        	$this->sidebar();
        }

        public function edit($uid = null, $pid = null) {

            if (!$uid) {
                throw new NotFoundException(__('Invalid comment'));
            }
        
            $comment = $this->Comment->findById($uid);
            if (!$comment) {
                throw new NotFoundException(__('Invalid comment'));
            }

            if ($this->request->is(array('post', 'put'))) {
            	echo $this->data['Comment']['id'];
                $this->Comment->id = $uid;
                if ($this->Comment->save($this->request->data)) {
                    $this->Flash->success(__('Your comment has been edited.'));
                    return $this->redirect(array('action' => 'view', $pid));
                }
                $this->Flash->error(__('Unable to edit your comment.'));
            }
        
            if (!$this->request->data) {
               $this->request->data = $comment;
            }
            $this->sidebar();
        }
        
        public function delete($cid = null, $pid = null) {

            if ($this->request->is('get')) {
            	//throw new MethodNotAllowedException();
                $this->Flash->error(__('Unauthorized access!'));
                return $this->redirect(array('controller'=>'posts', 'action'=>'index'));
            }

            if ($this->Comment->delete($cid)) {
            	$this->Flash->success(
            		__('Your comment has been deleted.')
            	);
            	return $this->redirect(array('action' => 'view', $cid, $pid));
            } else {
            	$this->Flash->error(
            		__('Comment could not be deleted.')
            	);
            }
        }

        public function delete_in_comments($id) {

            $this->Comment->deleteAll(array(
                "Comment.post_id" => "$id"
            ));
        }

        public function sidebar () {

            $id_logged = $_SESSION['id'];
            $sidebar = new UsersController;
            $sidebar->sidebar();

            $user_query = $this->Auth->user();
            $this->set('logged', $user_query);

            # count posts
            $count_posts_query = $this->Comment->find('count', array(
                'contain' => array('Post'),
                'conditions' => array(
                    'Post.user_id' => "$id_logged"
                )
            ));
            $this->set('postCount', $count_posts_query);
        }

        public function redirect_check_edit($id = null) {
            $check_edit = $this->Comment->find('first', array(
                'conditions' => array (
                    'Comment.id' => "$id"
                )
            ));
            $this->Session->write('check_edit', $check_edit);
        }

        public function redirect_edit($cid = null, $pid = null) {
            return $this->redirect(array('controller'=>'comments', 'action'=>'edit', $cid, $pid));
        }

      /*  public function redirect_view($puid = null, $pid = null) {
            return $this->redirect(array('controller'=>'comments', 'action'=>'view', $puid, $pid));
        }*/
    }

?>