
<?php

    App::import('Controller', 'Follows');
    App::import('Controller', 'Likes');
    App::import('Controller', 'Reposts');
    App::import('Controller', 'Comments');
    App::import('Controller', 'Users');

    class PostsController extends AppController {

        public $helpers = array('Html', 'Form', 'Paginator');

        public $paginate = array(
            'limit' => 10,
            'order' => 'Post.created DESC'
        );

        public function index($data = null) {

            $activation = $this->Auth->user('activation');

            if ($activation==0) {
                session_destroy();
                $this->Flash->error(__('Your account is not activated. Please check your email then activate.'));
                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
            } else {

                #Pagination
                $this->Paginator->settings = $this->paginate;
                $posts = $this->Paginator->paginate('Post');
                $this->set('posts', $posts);
                //  pr ($posts);

                #Reposting
                $id_logged = $this->Auth->user('id');
                $_SESSION['id'] = $id_logged;

                $repost_query = $this->Post->find('all', array(
                    'contain' => array('User','Repost')
                ));
                $this->set('reposts', $repost_query);

                #count likes
                $likes = $this->Post->find('all', array (
                    'contain' => array('Like')
                ));
                $this->set('likes', $likes);

                #count reposts
                $reposts = $this->Post->find('all', array (
                    'contain' => array('Repost')
                ));
                $this->set('reposts', $reposts);

                #count comments
                $comments = $this->Post->find('all', array (
                    'contain' => array('Comments')
                ));
                $this->set('comments', $comments);

                #check if liked or not
                $likers = $this->Post->find('all', array (
                    'contain' => array('User','Like'),
                    'order' => 'Post.created DESC'
                ));
                $this->set('likers', $likers);

                #check if reposted or not
                $reposters = $this->Post->find('all', array (
                    'contain' => array('User','Repost'),
                    'order' => 'Post.created DESC'
                ));
                $this->set('reposters', $reposters);
                $this->sidebar();
            }       
        }

        public function view($id = null) {

            if (!$id) {
                throw new NotFoundException(__('Invalid post'));
            }

            $post = $this->Post->findById($id);
            if (!$post) {
                throw new NotFoundException(__('Invalid post'));
            }
            $this->set('post', $post);
            $this->sidebar();
        }

        public function add() {

            if ($this->request->is('post')) {
                $this->request->data['Post']['user_id'] = $this->Auth->user('id');
                if ($this->Post->save($this->request->data)) {
                    $this->Flash->success(__('Your post has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                }
                $this->Flash->error(__('Unable to add your post.'));
            }
            $this->sidebar();
        }

        public function edit($id = null) {

            if (!$id) {
                throw new NotFoundException(__('Invalid post'));
            }
        
            $post = $this->Post->findById($id);
            if (!$post) {
               // throw new NotFoundException(__('Invalid post'));
            }
        
            if ($this->request->is(array('post', 'put'))) {
                $this->Post->id = $id;
                if ($this->Post->save($this->request->data)) {
                    $this->Flash->success(__('Your post has been updated.'));
                    return $this->redirect(array('action' => 'index'));
                }
                $this->Flash->error(__('Unable to update your post.'));
            }
        
            if (!$this->request->data) {
               $this->request->data = $post;
            }
            $this->sidebar();
        }
        
        public function delete($id) {

            if ($this->request->is('get')) {
            	$this->Flash->error(__('Unauthorized access!'));
                return $this->redirect(array('controller'=>'posts', 'action'=>'index'));
            }

            $post_to_delete = $this->Post->delete($id);
            if ($post_to_delete) {

                $delete_in_likes = new LikesController;
                $delete_in_likes->delete_in_likes($id);

                $delete_in_reposts = new RepostsController;
                $delete_in_reposts->delete_in_reposts($id);

                $delete_in_comments = new CommentsController;
                $delete_in_comments->delete_in_comments($id);

            	$this->Flash->success(
            		__('Post has been deleted')
            	);
            } else {
            	$this->Flash->error(
            		__('Unable to delete post')
            	);
            }
            return $this->redirect(array('action' => 'index'));
        }

        public function sidebar () {

            $id_logged = $_SESSION['id'];
            $sidebar = new UsersController;
            $sidebar->sidebar();

            $user_query = $this->Auth->user();
            $this->set('logged', $user_query);
           // pr ($user_query);

            # count posts
            $count_posts_query = $this->Post->find('count', array(
                'conditions' => array(
                    'Post.user_id' => "$id_logged"
                )
            ));

            unset($_SESSION['countposts']);
            $_SESSION['countposts'] = $count_posts_query;

            # count followers and followings
            $Follows = new FollowsController;
            $Follows->count_followers(); 
            $Follows->count_followings();
        }

        public function allpost () {

            $id_logged = $_SESSION['id'];
            $allposts = $this->Post->find('all', array(
            	'order' => 'Post.created DESC',
                'conditions' => array (
                    'Post.user_id' => "$id_logged"
                )
            ));
            $this->set('allpost', $allposts);
        	$this->sidebar();
        }

        public function PostOwner($uid) {
            $PostOwner = new UsersController;
            $PostOwner->PostOwner($uid);
        }

        public function redirect_check_edit($id=null) {
            $check_edit = $this->Post->find('first', array(
                'conditions' => array (
                    'Post.id' => "$id"
                )
            ));
            $this->Session->write('check_edit', $check_edit);
        }

        public function redirect_edit($id=null) {
            return $this->redirect(array('controller'=>'posts', 'action'=>'edit', $id));
        }
    }

?>