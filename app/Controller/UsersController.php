
<?php

    App::uses('AppController', 'Controller');
    App::uses('Folder', 'Utility');
    App::uses('File', 'Utility');
    App::import('Controller', 'Follows');
    App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

    class UsersController extends AppController {

        public $helpers = array('Html', 'Form', 'Paginator');

        public function beforeFilter() {
            parent::beforeFilter();
            $this->Auth->allow(array('add', 'logout', 'activate', 'forgot_password', 'commit_password_change'));
        }

        public function index() {
            $this->User->recursive = 0;
            $this->set('users', $this->paginate());
            $this->layout = false;

            $this->redirect(array('action'=>'login'));
        }

        public function add() {
            if ($this->request->is('post')) {
                $this->User->create();

                # send email activation
                $toBeEmailed = $this->request->data['User']['email'];

                $Email = new CakeEmail();
                $Email->from(array('microblog0001@gmail.com' => 'Microblog'));
                $Email->to($toBeEmailed);
                $Email->emailFormat('html');
                $Email->subject('Email Activation');
                $Email->send('Your account in Microblog has been successfully created. <br/>Please click on the link to activate your account. <br/><br/> <a href="localhost/test/Microblog_cakephp_2/users/activate/'.$toBeEmailed.'">Activate Account</a>');

                if ($this->User->save($this->request->data)) {
                    $this->Flash->success(__('The user has been saved. Please check your email for activation.'));
                    return $this->redirect(array('action' => 'login'));
                } else {
                    $this->Flash->error(
                        __('The user could not be saved. Please, try again.')
                    );
                }
            }
        }

        public function edit($id = null) {
            
            if (!$id) {
                throw new NotFoundException(__('Invalid User'));
            }
        
            $user = $this->User->findById($id);

            $changeEmail = 0;   // 0 - not changed; 1 - changed;
            $changePic = 0;     // 0 - not changed; 1 - changed; 2 - null;

            if ($this->request->is(array('post', 'put'))) {
                $this->User->id = $id;

                # image
                $file = $this->data['User']['image'];
                $ext = pathinfo(($file['name']), PATHINFO_EXTENSION);
                $file_ext = array ('png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF');
                if(!empty($file['name'])) {

                    //$newfilename = round(microtime(true)) . '.' . end($ext);
                    if (in_array($ext, $file_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img\users\\' . $file['name']);
                        $this->request->data['User']['image'] = $file['name'];
                        $changePic = 1;
                    }
                } elseif ($file['name']==null) {
                    $this->request->data['User']['image'] = $file['name'];
                    $changePic = 2;
                }

                # email address
                if ($this->Auth->User('email') != $this->data['User']['email']) {
                    $changeEmail = 1;
                } else {
                    $changeEmail = 0;
                }

                if ($changePic == 0 || $changePic == 2 && $changeEmail == 0) {      // Pic NOT changed; Email NOT changed;

                    if (in_array($ext, $file_ext) || $changePic==2) {
                        $this->request->data['User']['image'] = $user['User']['image'];
                        if ($this->User->save($this->request->data)) {
                            $this->Flash->success(__('Successfully updated your profile.'));
                            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                        } else {
                            $this->Flash->error(__('Unable to update profile'));
                        }
                    } else {
                        $this->Flash->error(__('The file you have chosen is not an image.'));
                    }
                } else if ($changePic == 0 || $changePic == 2 && $changeEmail == 1) {       // Pic NOT changed; Email changed;
                    if (in_array($ext, $file_ext) || $changePic==2) {
                        if ($this->User->save($this->request->data)) {
                            $this->request->data['User']['activation'] = 0;
                            $toBeEmailed = $this->data['User']['email'];

                            $Email = new CakeEmail();
                            $Email->from(array('microblog0001@gmail.com' => 'Microblog'));
                            $Email->to($toBeEmailed);
                            $Email->emailFormat('html');
                            $Email->subject('Email Activation');
                            $Email->send('You have updated your email. <br/>Please click on the link to re-activate your account. <br/><br/> <a href="localhost/test/Microblog_cakephp_2/users/activate/'.$toBeEmailed.'">Activate Account</a>');

                            $this->request->data['User']['image'] = $user['User']['image'];
                            if ($this->User->save($this->request->data)) {
                                $this->Flash->success(__('Successfully updated your profile. Please check your email to activate account'));
                                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                            } else {
                                $this->Flash->error(__('Unable to update profile'));
                            }
                        } else {
                            $this->Flash->error(__('Unable to update profile'));
                        }
                    } else {
                        $this->Flash->error(__('The file you have chosen is not an image.'));
                    }
                } else if ($changePic == 1 && $changeEmail == 0) {      // Pic changed; Email NOT changed;

                    if (in_array($ext, $file_ext)) {
                        if ($this->User->save($this->request->data)) {
                            $this->Flash->success(__('Successfully updated your profile.'));
                            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                        } else {
                            $this->Flash->error(__('Unable to update profile'));
                        }
                    } else {
                        $this->Flash->error(__('The file you have chosen is not an image.'));
                    }
                } else if ($changePic == 1 && $changeEmail == 1) {      // Pic changed; Email changed;

                    if (in_array($ext, $file_ext)) {
                        if ($this->User->save($this->request->data)) {
                            $this->request->data['User']['activation'] = 0;
                            $toBeEmailed = $this->data['User']['email'];

                            $Email = new CakeEmail();
                            $Email->from(array('microblog0001@gmail.com' => 'Microblog'));
                            $Email->to($toBeEmailed);
                            $Email->emailFormat('html');
                            $Email->subject('Email Activation');
                            $Email->send('You have updated your email. <br/>Please click on the link to re-activate your account. <br/><br/> <a href="localhost/test/Microblog_cakephp_2/users/activate/'.$toBeEmailed.'">Activate Account</a>');

                            if ($this->User->save($this->request->data)) {
                                $this->Flash->success(__('Successfully updated your profile. Please check your email to activate account'));
                                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                            } else {
                                $this->Flash->error(__('Unable to update profile'));
                            }
                        } else {
                            $this->Flash->error(__('Unable to update profile'));
                        }
                    } else {
                        $this->Flash->error(__('The file you have chosen is not an image.'));   
                    }
                }
            }
        
            if (!$this->request->data) {
               $this->request->data = $user;
            }

            $this->sidebar();
        }

        public function forgot_password() {

            if ($this->request->is('post')) {

                $current_email = $this->data['User']['email_pass'];
                $new_pass = $this->data['User']['new_password'];
                $confirm_new_pass = $this->data['User']['confirm_new_password'];

                $id = $this->User->find('first', array(
                    'conditions' => array(
                        'User.email' => "$current_email"
                    )
                ));

                if (!$id) {
                    $this->Flash->error(__('Email does not exist'));
                }

                if ($new_pass == $confirm_new_pass) {

                    #rand string act_link generation
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    for ($i = 0; $i < 30; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }

                    $this->User->updateAll(
                        array('act_link' => "'$randomString'"),
                        array('email' => $current_email)
                    );

                   // echo $randomString;

                    $uid = $id['User']['id'];

                    $Email = new CakeEmail();
                    $Email->from(array('microblog0001@gmail.com' => 'Microblog'));
                    $Email->to($current_email);
                    $Email->emailFormat('html');
                    $Email->subject('Verify Forgot Password');
                    $Email->send('Please click the link to verify activity. <br/><br/> <a href="localhost/test/Microblog_cakephp_2/users/commit_password_change/'.$uid.'/'.$randomString.'">Verify Password Change</a>');

                    unset($_SESSION['forgot_password']);
                    $_SESSION['forgot_password'] = $new_pass;
                    unset($_SESSION['current_email']);
                    $_SESSION['current_email'] = $current_email;

                    $this->Flash->success(__('Please check your email and verify password change.'));
                    return $this->redirect(array('action'=>'login'));
                } else {
                    $this->Flash->error(__('Passwords do not match'));
                }
            }
        }

        public function commit_password_change ($id = null) {

           // echo $this->params['pass'][1];
            $act_link = $this->User->find('first', array(
                'conditions' => array (
                    'act_link' => $this->params['pass'][1]
                )
            ));
           // pr($act_link);

            if ($this->params['pass'][1] == $act_link['User']['act_link']) {
                $new_pass = $_SESSION['forgot_password'];
                $current_email = $_SESSION['current_email'];

                $user = $this->User->findById($id);

                $this->User->create();
                $this->User->id = $id;
                $this->request->data['password'] = $new_pass;
                $this->request->data['activation'] = 1;
                $this->request->data['image'] = $user['User']['image'];

                if ($this->User->save($this->request->data)) {
                    $this->Flash->success(__('Password successfully changed.'));
                    return $this->redirect(array('action' => 'login'));
                } else {
                    $this->Flash->error(__('Failed to change password.'));
                    return $this->redirect(array('action' => 'forgot_password'));
                }
            } else {
                $this->Flash->error(__('Unable to change password.'));
            }
        }

        public function view($id = null) {

            if (!$id) {
                throw new NotFoundException(__('Invalid user'));
            }
            $user = $this->User->findById($id);
            if (!$user) {
                throw new NotFoundException(__('Invalid user'));
            }
            $this->set('user', $user);

            $user_id = $user['User']['id'];

            # count posts
            $count_posts_query = $this->User->find('count', array(
                'conditions' => array(
                    'Post.user_id' => "$user_id"
                )
            ));

            unset($_SESSION['view_countposts']);
            $_SESSION['view_countposts'] = $count_posts_query;

            # count followers and followings
            $Follows = new FollowsController;
            $Follows->count_followers();
            $Follows->count_followings();
            $Follows->view_count_followers($user_id); 
            $Follows->view_count_followings($user_id); 
            $Follows->you_followed($user_id);
            $Follows->follows_you($user_id);

            echo $this->request->query($id);

            # getting user posts
            $query = $this->User->find('all', array (
                'contain' => array ('Post','Like','Repost','Comment'),
                'conditions' => array (
                    "User.id" => "$user_id"
                ),
                'order' => 'Post.created DESC'
            ));
            $this->set('posts', $query);
            $this->sidebar();
        }

        public function search () {
            $this->sidebar();
        }

        public function search_result() {
            $id_logged = $_SESSION['id'];
            $keyword = $this->request->query('keyword');

            $this->paginate = array('User'=> array(
                'limit'=>15, 
                'fields' => array ('User.id', 'User.username','User.firstname','User.lastname','User.image'),
                'conditions' => array (
                    'OR' => array (
                        "User.username LIKE" => "%$keyword%", 
                        "User.firstname LIKE" => "%$keyword%", 
                        "User.lastname LIKE" => "%$keyword%"
                    )
                )
            ));
            $query = $this->paginate('User');
            $this->set('users', $query);
            $this->sidebar();
        }

        public function sidebar () {

            $id_logged = $_SESSION['id'];

            $user_query = $this->User->findById($id_logged);
            $this->set('logged', $user_query);
         //   $this->Session->write('user_query', $user_query);

            unset($_SESSION['firstname']);
            $_SESSION['firstname'] = $user_query['User']['firstname'];
            unset($_SESSION['lastname']);
            $_SESSION['lastname'] = $user_query['User']['lastname'];
            unset($_SESSION['username']);
            $_SESSION['username'] = $user_query['User']['username'];
            unset($_SESSION['email']);
            $_SESSION['email'] = $user_query['User']['email'];
            unset($_SESSION['birthday']);
            $_SESSION['birthday'] = $user_query['User']['birthday'];
            unset($_SESSION['image']);
            $_SESSION['image'] = $user_query['User']['image'];
        }

        public function activate ($email = null) {

            $this->User->updateAll(
                array('activation' => 1),
                array('email' => $email)
            );

            $this->Flash->success(__('Successfully activated account.'));
            return $this->redirect(array('action' => 'login'));
        }

        public function PostOwner($uid) {

            $postowners = $this->User->find('all', array(
                'conditions' => array(
                    'User.id' => "$uid"
                )
            ));

            foreach ($postowners as $postowner) {
                if ($postowner['User']['id']==$uid) {
                    unset($_SESSION['user_username']);
                    $_SESSION['user_username'] = $postowner['User']['username'];
                    unset($_SESSION['user_image']);
                    $_SESSION['user_image'] = $postowner['User']['image'];
                }
            }
        }

        public function login() {

            if ($this->request->is('post')) {
                if ($this->Auth->login()) {
                    return $this->redirect(array('controller'=>'posts','action'=>'index'));
                } else {
                    $this->Flash->error(__('Invalid username or password, try again'));
                }
            }
        }

        public function logout() {

           // $this->Session->delete('followingz');
            $this->redirect($this->Auth->logout());
            $this->Auth->logout();
            $this->Session->destroy();
            $this->redirect(array('action' => 'login'));
        }

        public function redirect_edit() {
            return $this->redirect(array('controller'=>'users', 'action'=>'edit', $_SESSION['id']));
        }
    }

?>