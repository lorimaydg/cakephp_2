
<?php

    App::import('Controller', 'Users');
    
	class LikesController extends AppController {

        public function like($id = null) {

            $id_logged = $_SESSION['id'];

            echo $num_likes_query;
            $likers_query = $this->Like->find('all', array(
                'conditions' => array(
                    "Like.user_id" => "$id_logged",
                    "Like.post_id" => "$id"
                )
            ));

            if (!$likers_query) {
                $this->Like->create();
                $this->request->data['Like']['user_id'] = $id_logged;
                $this->request->data['Like']['post_id'] = $id;

                if ($this->Like->save($this->request->data)) {
                    return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                }
            } else {
                $this->unlike($id);
            }
        }

        public function unlike($id = null) {

            $id_logged = $_SESSION['id'];
            $this->Like->deleteAll(array(
                "Like.user_id" => "$id_logged",
                "Like.post_id" => "$id"
            ));
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }

        public function like_allpost ($id = null) {

            $id_logged = $_SESSION['id'];

            echo $num_likes_query;
            $likers_query = $this->Like->find('all', array(
                'conditions' => array(
                    "Like.user_id" => "$id_logged",
                    "Like.post_id" => "$id"
                )
            ));

            if (!$likers_query) {
                $this->Like->create();
                $this->request->data['Like']['user_id'] = $id_logged;
                $this->request->data['Like']['post_id'] = $id;

                if ($this->Like->save($this->request->data)) {
                    return $this->redirect(array('controller' => 'posts', 'action' => 'allpost'));
                }
            } else {
                $this->unlike_allpost($id);
            }
        }

        public function unlike_allpost($id = null) {

            $id_logged = $_SESSION['id'];
            $this->Like->deleteAll(array(
                "Like.user_id" => "$id_logged",
                "Like.post_id" => "$id"
            ));
            return $this->redirect(array('controller' => 'posts', 'action' => 'allpost'));
        }

        public function like_view ($pid, $uid) {

            $id_logged = $_SESSION['id'];

            $likers_query = $this->Like->find('all', array(
                'conditions' => array(
                    "Like.user_id" => "$id_logged",
                    "Like.post_id" => "$pid"
                )
            ));

            if (!$likers_query) {
                $this->Like->create();
                $this->request->data['Like']['user_id'] = $id_logged;
                $this->request->data['Like']['post_id'] = $pid;

                if ($this->Like->save($this->request->data)) {
                    return $this->redirect(array('controller' => 'posts', 'action' => 'view', $pid, $uid));
                }
            } else {
                $this->unlike_view($pid, $uid);
            }
        }

        public function unlike_view($pid, $uid) {

            $id_logged = $_SESSION['id'];
            $this->Like->deleteAll(array(
                "Like.user_id" => "$id_logged",
                "Like.post_id" => "$pid"
            ));
            return $this->redirect(array('controller' => 'posts', 'action' => 'view', $pid, $uid));
        }

        public function likers ($id = null) {

            $id_logged = $_SESSION['id'];

            $likers = $this->Like->find('all', array(
                'contain' => array('Post','User'),
                'conditions' => array(
                    "Like.post_id" => "$id"
                ),
                'order' => 'Like.created DESC'
            ));

            $this->set('likers',$likers);

            $this->sidebar();
        }

        public function likes_notification () {

            $id_logged = $_SESSION['id'];

            $fetch_like_notifs = $this->Like->find('all', array(
                'contain' => array('User', 'Post'),
                'order' => 'Like.created DESC',
                'conditions' => array(
                    'Like.user_id = User.id',
                    'Like.post_id = Post.id',
                    'Post.user_id' => "$id_logged"
                )
            ));

            $this->set('like_notifs', $fetch_like_notifs);
            $this->sidebar();
        }

        public function delete_in_likes($id) {

            $this->Like->deleteAll(array(
                "Like.post_id" => "$id"
            ));
        }

        public function sidebar () {

            $id_logged = $_SESSION['id'];
            $sidebar = new UsersController;
            $sidebar->sidebar();

            $user_query = $this->Auth->user();
            $this->set('logged', $user_query);
        }
    }

?>