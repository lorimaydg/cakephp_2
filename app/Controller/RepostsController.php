
<?php

    App::import('Controller', 'Users');
    
	class RepostsController extends AppController {

        public function repost($id = null) {

            $id_logged = $_SESSION['id'];
            $query = $this->Repost->find('all', array(
                'conditions' => array(
                    "Repost.user_id" => "$id_logged",
                    "Repost.post_id" => "$id"
                )
            ));

            if (!$query) {
                $this->Repost->create();
                $this->request->data['Repost']['user_id'] = $id_logged;
                $this->request->data['Repost']['post_id'] = $id;

                if ($this->Repost->save($this->request->data)) {
                    return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                }
            } else {
                $this->unrepost($id);
            }
        }

        public function unrepost($id = null) {

            $id_logged = $_SESSION['id'];
            $this->Repost->deleteAll(array(
                "Repost.user_id" => "$id_logged",
                "Repost.post_id" => "$id"
            ));

            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }

        public function repost_allpost($id = null) {

            $id_logged = $_SESSION['id'];
            $query = $this->Repost->find('all', array(
                'conditions' => array(
                    "Repost.user_id" => "$id_logged",
                    "Repost.post_id" => "$id"
                )
            ));

            if (!$query) {
                $this->Repost->create();
                $this->request->data['Repost']['user_id'] = $id_logged;
                $this->request->data['Repost']['post_id'] = $id;

                if ($this->Repost->save($this->request->data)) {
                    return $this->redirect(array('controller' => 'posts', 'action' => 'allpost'));
                }
            } else {
                $this->unrepost_allpost($id);
            }
        }

        public function unrepost_allpost($id = null) {

            $id_logged = $_SESSION['id'];
            $this->Repost->deleteAll(array(
                "Repost.user_id" => "$id_logged",
                "Repost.post_id" => "$id"
            ));

            return $this->redirect(array('controller' => 'posts', 'action' => 'allpost'));
        }

        public function repost_view($pid,$uid) {

            $id_logged = $_SESSION['id'];
            $query = $this->Repost->find('all', array(
                'conditions' => array(
                    "Repost.user_id" => "$id_logged",
                    "Repost.post_id" => "$pid"
                )
            ));

            if (!$query) {
                $this->Repost->create();
                $this->request->data['Repost']['user_id'] = $id_logged;
                $this->request->data['Repost']['post_id'] = $pid;

                if ($this->Repost->save($this->request->data)) {
                    return $this->redirect(array('controller' => 'posts', 'action' => 'view', $pid, $uid));
                }
            } else {
                $this->unrepost_view($pid,$uid);
            }
        }

        public function unrepost_view($pid,$uid) {

            $id_logged = $_SESSION['id'];
            $this->Repost->deleteAll(array(
                "Repost.user_id" => "$id_logged",
                "Repost.post_id" => "$pid"
            ));

            return $this->redirect(array('controller' => 'posts', 'action' => 'view', $pid, $uid));
        }

        public function reposters ($id = null) {

            $id_logged = $_SESSION['id'];

            $reposter = $this->Repost->find('all', array(
                'contain' => array('User','Post'),
                'conditions' => array(
                    "Repost.post_id" => "$id"
                ),
                'order' => 'Repost.created DESC'
            ));

            $this->set('reposters',$reposter);
            $this->sidebar();
        }

        public function reposts_notification () {

            $id_logged = $_SESSION['id'];

            $fetch_repost_notifs = $this->Repost->find('all', array(
                'contain' => array('User', 'Post'),
                'order' => 'Repost.created DESC',
                'conditions' => array(
                    'Repost.user_id = User.id',
                    'Repost.post_id = Post.id',
                    'Post.user_id' => "$id_logged"
                )
            ));

            $this->set('repost_notifs', $fetch_repost_notifs);
            $this->sidebar();
        }

        public function delete_in_reposts($id) {

            $this->Repost->deleteAll(array(
                "Repost.post_id" => "$id"
            ));
        }

        public function sidebar () {

            $id_logged = $_SESSION['id'];
            $sidebar = new UsersController;
            $sidebar->sidebar();

            $user_query = $this->Auth->user();
            $this->set('logged', $user_query);
        }
    }

?>