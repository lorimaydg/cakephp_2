
<?php

    App::import('Controller', 'Users');

	class FollowsController extends AppController {

        public function follow($id = null) {

            $id_logged = $_SESSION['id'];
            $query = $this->Follow->find('all', array(
                'conditions' => array(
                    "Follow.user_id" => "$id_logged",
                    "Follow.follow_id" => "$id"
                )
            ));

            if (!$query) {
                $this->Follow->create();
                $this->request->data['Follow']['user_id'] = $id_logged;
                $this->request->data['Follow']['follow_id'] = $id;

                if ($this->Follow->save($this->request->data)) {
                    $this->count_followers();
                    $this->count_followings();
                    $this->view_count_followers($id);
                    $this->view_count_followings($id);
                    $this->you_followed($id);
                    $this->follows_you($id);
                    return $this->redirect(array('controller' => 'users', 'action' => 'view', $id));
                }
            } else {
                $this->unfollow($id);
            }
        }

        public function unfollow($id = null) {

            $id_logged = $_SESSION['id'];
            $this->Follow->deleteAll(array(
                "Follow.user_id" => "$id_logged",
                "Follow.follow_id" => "$id"
            ));
            return $this->redirect(array('controller' => 'users', 'action' => 'view', $id));
        }

        public function followers() {

            $id_logged = $_SESSION['id'];

            # displaying followers
            $followers_query = $this->Follow->find('all', array (
                'contain' => array('User'),
                'conditions' => array(
                    'Follow.follow_id' => "$id_logged"
                )
            ));
            $this->set('followers', $followers_query);
            $this->Session->write('followerz', $followers_query);
            $this->sidebar();
        }

        public function following() {

            $id_logged = $_SESSION['id'];

            $followings_query = $this->Follow->find('all', array(
                'conditions' => array(
                    'Follow.user_id' => "$id_logged"
                )
            ));
            $this->set('followings', $followings_query);
            $this->Session->write('followingz', $followings_query);
            $this->sidebar();
        }

        public function sidebar () {

            $id_logged = $_SESSION['id'];
            $sidebar = new UsersController;
            $sidebar->sidebar();

            $user_query = $this->Auth->user();
            $this->set('logged', $user_query);
        }

        public function follow_notification () {

            $id_logged = $_SESSION['id'];

            $fetch_follow_notifs = $this->Follow->find('all', array(
                'order' => 'Follow.created DESC',
                'conditions' => array(
                    'Follow.follow_id' => "$id_logged"
                )
            ));
            $this->set('follow_notifs', $fetch_follow_notifs);
            $this->sidebar();
        }

        public function count_followers () {

            $id_logged = $_SESSION['id'];

            $count_followers_query = $this->Follow->find('count', array(
                'conditions' => array(
                    'Follow.follow_id' => "$id_logged"
                )
            ));
            unset($_SESSION['countFollowers']);
            $_SESSION['countFollowers'] = $count_followers_query;
        }

        public function count_followings () {

            $id_logged = $_SESSION['id'];

            $count_followings_query = $this->Follow->find('count', array(
                'conditions' => array(
                    'Follow.user_id' => "$id_logged"
                )
            ));
            unset($_SESSION['countFollowings']);
            $_SESSION['countFollowings'] = $count_followings_query;
        }

        public function view_count_followers ($id) {
            
            $count_followers_query = $this->Follow->find('count', array(
                'conditions' => array(
                    'Follow.follow_id' => "$id"
                )
            ));
            unset($_SESSION['view_countFollowers']);
            $_SESSION['view_countFollowers'] = $count_followers_query;
        }

        public function view_count_followings ($id) {

            $count_followings_query = $this->Follow->find('count', array(
                'conditions' => array(
                    'Follow.user_id' => "$id"
                )
            ));
            unset($_SESSION['view_countFollowings']);
            $_SESSION['view_countFollowings'] = $count_followings_query;
        }

        public function you_followed ($id) {

            $id_logged = $_SESSION['id'];

            $you_followed = $this->Follow->find('count', array(
                'conditions' => array(
                    'Follow.user_id' => "$id_logged",
                    'Follow.follow_id' => "$id"
                )
            ));
            unset($_SESSION['you_followed']);
            $_SESSION['you_followed'] = $you_followed;
        }

        public function follows_you ($id) {

            $id_logged = $_SESSION['id'];

            $follows_you = $this->Follow->find('count', array(
                'conditions' => array(
                    'Follow.user_id' => "$id",
                    'Follow.follow_id' => "$id_logged"
                )
            ));
            unset($_SESSION['follows_you']);
            $_SESSION['follows_you'] = $follows_you;
        }
    }

?>