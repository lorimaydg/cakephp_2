
<?php echo $this->Html->css(array('home','search')); ?>

<div class="container-home">

    <br/>
    <div class="header-menu">

        <?php echo $this->Html->link(
            'Home',
            array('controller' => 'posts', 'action' => 'index')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Notifications',
            array('controller' => 'follows', 'action' => 'follow_notification')
            ); 
        ?> 
    
    </div>

    <br/><br/>
    <h3 class="label">Liked by:</h3><br/>

    <?php if ($likers==null) : echo "<label style='position:relative;left:30px;width:200px'>No Likes</label>"; ?>
    <?php else : ?>
    <?php foreach ($likers as $liker): ?>

    <br/>
    <div class="result-box">

        <div class="image">
            <?php echo $this->Html->image('users/'.$liker['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
        </div>
        <div class="name">
            <?php echo h($liker['User']['firstname'])." ".h($liker['User']['lastname']); ?>
        </div>
        <div class="username">
            <?php echo h($liker['User']['username']) ?>
        </div> 
        <div class="view-profile">
            <?php
                echo $this->Html->link(
                    'View Profile',
                    array('controller' => 'users', 'action' => 'view', $liker['User']['id'])
                );
            ?>
        </div>
    </div>

    <?php endforeach; ?>
    <?php endif; ?>
    <?php unset($liker); ?>

</div>
