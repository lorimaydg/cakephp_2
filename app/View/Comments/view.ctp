
<?php echo $this->Html->css('home'); ?>
<?php $id_logged = $_SESSION['id']; ?>
<?php //pr($comments) ?>

<div class="container-home">

    <br/>
    <div class="header-menu">

        <?php echo $this->Html->link(
            'Back',
            array('controller' => 'posts', 'action' => 'index')
            ); 
        ?>
        <!-- <a href="javascript:history.go(-1)">Back</a> -->

        <br/><br/><br/>

        <h3>Comments</h3>

        <?php echo $this->Html->link(
            'Add Comment',
            array('controller' => 'comments', 'action' => 'add', $this->params['pass'][0], $this->params['pass'][1])
            ); 
        ?> <br/><br/><br/>
    </div>

    <?php if ($comments==null) : echo "<label style='position:relative;left:30px;width:200px'>No Comments</label>"; ?>
    <?php else : ?>

    <?php $uid = $this->params['pass'][0]; $pid = $this->params['pass'][1]; ?>
    <div class="post-box">

        <div class="image">
            <?php echo $this->Html->image('users/'.$_SESSION['user_image'], array('height' => '70px', 'width' => '70px')); ?>
        </div>

        <div class="user">
            <b><?php echo h($_SESSION['user_username']); ?></b>
        </div>

        <div class="date-created">
            <?php echo h($comments[0]['Post']['created']); ?>
        </div>

        <div class="body">
            <?php echo h($comments[0]['Post']['body']); ?>
        </div>
        <?php if ($this->params['pass'][0]==$id_logged) : ?>
        <div class="edit-delete">
            <?php
                echo $this->Html->link(
                    'Edit',
                    array('controller' => 'posts', 'action' => 'edit', $this->params['pass'][1])
                );
            ?>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('controller' => 'posts', 'action' => 'delete', $this->params['pass'][1]),
                    array('confirm' => 'Are you sure?')
                );
            ?>
        </div>
        <?php endif; ?>
    </div><br/>

    <?php foreach ($comments as $comment): ?>

        <div class="post-box2">

            <div class="image">
                <?php echo $this->Html->image('users/'.$comment['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
            </div>

            <div class="user">
                <b><?php echo h($comment['User']['username']); ?></b>
            </div>

            <div class="date-created">
                <?php echo h($comment['Comment']['created']); ?>
            </div>

            <div class="body">
                <?php echo h($comment['Comment']['body']); ?>
            </div>

            <div>
                <?php if ($comment['Comment']['user_id']==$id_logged) : ?>
                <div class="edit-delete">
                    <?php
                        echo $this->Html->link(
                            'Edit',
                            array('action' => 'edit', $comment['Comment']['id'], $comment['Post']['id'])
                        );
                    ?>
                    <?php
                        echo $this->Form->postLink(
                            'Delete',
                            array('action' => 'delete', $comment['Comment']['id'], $comment['Post']['id']),
                            array('confirm' => 'Are you sure?')
                        );
                    ?>
                </div>
                <?php endif; ?>
            </div>
        </div><br/>
    <?php endforeach; ?>
    <?php endif; ?>
    <?php unset($allposts); ?>

</div>

<style type="text/css">
    
    .post-box2 {
        border: 1px solid #ffffff;
        width: 600px;
        height: 100px;
        position: relative;
        left: 100px;
        background: lightgray;
    }

</style>