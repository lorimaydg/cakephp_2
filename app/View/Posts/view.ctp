
<?php echo $this->Html->css('home'); ?>

<div class="container-home">

    <br/>

    <a href="javascript:history.go(-1)">Back</a>
    <!-- <?php echo $this->Html->link(
	    'Back',
	    array('controller' => 'users', 'action' => 'view', $this->params['pass'][1])
	    ); 
	?> -->

	<br/><br/><br/>

		<?php //pr($post) ?>

        <div class="post-box">
            
            <div class="image">
                <?php echo $this->Html->image('users/'.$post['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
            </div>

            <div class="user">
                <b><?php echo h($post['User']['username']); ?></b>
            </div>

            <div class="date-created">
                <?php echo h($post['Post']['created']); ?>
            </div>

            <div class="body">
                <?php echo h($post['Post']['body']); ?>
            </div>

            <div>
                <div class="like-repost">

                    <!-- Count likes -->
                    <?php
                        echo $this->Html->link(
                            count($post['Like']),
                            array('controller' => 'likes','action' => 'likers', $post['Post']['id'])
                        );
                    ?>

                    <!-- Like or Unlike display -->
                    <?php
                        $liked = 0;
                        $post2 = $post['Like'];
                        foreach ($post2 as $allpost2) {
                            if ($post['Post']['id']==$allpost2['post_id'] && $logged['id']==$allpost2['user_id']) {
                                echo $this->Html->link(
                                    'Unlike',
                                    array('controller' => 'likes','action' => 'like_view', $post['Post']['id'], $post['User']['id'])
                                );
                                $liked = 1; 
                            } 
                        }
                        if ($liked==0) {
                            echo $this->Html->link(
                                'Like',
                                array('controller' => 'likes','action' => 'like_view', $post['Post']['id'], $post['User']['id'])
                            );
                        }
                    ?> &nbsp;

                    <!-- Count reposts -->
                    <?php
                        echo $this->Html->link(
                            count($post['Repost']),
                            array('controller' => 'reposts','action' => 'reposters', $post['Post']['id'])
                        );
                    ?>

                    <!-- Repost or Unrepost display -->
                    <?php
                        $reposted = 0;
                        $post2 = $post['Repost'];
                        foreach ($post2 as $allpost2) {
                            if ($post['Post']['id']==$allpost2['post_id'] && $logged['id']==$allpost2['user_id']) {
                                echo $this->Html->link(
                                    'Unrepost',
                                    array('controller' => 'reposts','action' => 'repost_view', $post['Post']['id'], $post['User']['id'])
                                );
                                $reposted = 1; 
                            } 
                        }
                        if ($reposted==0) {
                            echo $this->Html->link(
                                'Repost',
                                array('controller' => 'reposts','action' => 'repost_view', $post['Post']['id'], $post['User']['id'])
                            );
                        }
                    ?> &nbsp;

                    <!-- Count comments -->
                    <?php echo count($post['Comments']); ?>

                    <!-- Comments -->
                    <?php
                        echo $this->Html->link(
                            'Comments',
                            array('controller' => 'comments','action' => 'view', $post['User']['id'], $post['Post']['id'])
                        );
                    ?>
                </div>
            </div>
            
        </div><br/>
    <?php unset($post); ?>

</div>

<style type="text/css">
    .like-repost {
        position: relative;
        left: 110px;
        top: -37px;
        width: 300px;
    }
</style>