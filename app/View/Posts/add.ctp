
<br/>
<a href="javascript:history.go(-1)">Back</a>
<br/>
<h1>Add Post</h1>

<?php
    echo $this->Form->create('Post');
    echo $this->Form->input(
    	'body', 
    	array(
    		'rows' => '3',
    		'label'=>'Post', 
    		'placeholder'=>'Write a post (140 characters only)'
    	) 
    );
    echo $this->Form->end('Post');
?>

<!-- <?php //echo $this->Html->link(
    //'Cancel',
    //array('controller' => 'posts', 'action' => 'index')
    ); 
?> -->