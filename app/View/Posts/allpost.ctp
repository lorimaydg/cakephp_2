
<?php echo $this->Html->css('home'); ?>

<div class="container-home">

    <br/>

    <div class="header-menu">

        <?php echo $this->Html->link(
            'Home',
            array('controller' => 'posts', 'action' => 'index')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Notifications',
            array('controller' => 'follows', 'action' => 'follow_notification')
            ); 
        ?> 
    
    </div>

    <br/><br/>
    <h3 class="label">All Posts</h3><br/>

    <?php if ($allpost==null) : echo "<label style='position:relative;left:30px;width:200px'>No Posts</label>"; ?>
    <?php else : ?>
    <?php foreach ($allpost as $allposts): ?>

        <div class="post-box">
            
            <div class="image">
                <?php echo $this->Html->image('users/'.$allposts['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
            </div>

            <div class="user">
                <b><?php echo h($allposts['User']['username']); ?></b>
            </div>

            <div class="date-created">
                <?php echo h($allposts['Post']['created']); ?>
            </div>

            <div class="body">
                <?php echo h($allposts['Post']['body']); ?>
            </div>

            <div>
                <div class="like-repost">

                    <!-- Count likes -->
                    <?php
                        echo $this->Html->link(
                            count($allposts['Like']),
                            array('controller' => 'likes','action' => 'likers', $allposts['Post']['id'])
                        );
                    ?>

                    <!-- Like or Unlike display -->
                    <?php
                        $liked = 0;
                        $allposts2 = $allposts['Like'];
                        foreach ($allposts2 as $allpost2) {
                            if ($allposts['Post']['id']==$allpost2['post_id'] && $logged['id']==$allpost2['user_id']) {
                                echo $this->Html->link(
                                    'Unlike',
                                    array('controller' => 'likes','action' => 'like_allpost', $allposts['Post']['id'])
                                );
                                $liked = 1; 
                            } 
                        }
                        if ($liked==0) {
                            echo $this->Html->link(
                                'Like',
                                array('controller' => 'likes','action' => 'like_allpost', $allposts['Post']['id'])
                            );
                        }
                    ?> &nbsp;

                    <!-- Count reposts -->
                    <?php
                        echo $this->Html->link(
                            count($allposts['Repost']),
                            array('controller' => 'reposts','action' => 'reposters', $allposts['Post']['id'])
                        );
                    ?>

                    <!-- Repost or Unrepost display -->
                    <?php
                        $reposted = 0;
                        $allposts2 = $allposts['Repost'];
                        foreach ($allposts2 as $allpost2) {
                            if ($allposts['Post']['id']==$allpost2['post_id'] && $logged['id']==$allpost2['user_id']) {
                                echo $this->Html->link(
                                    'Unrepost',
                                    array('controller' => 'reposts','action' => 'repost_allpost', $allposts['Post']['id'])
                                );
                                $reposted = 1; 
                            } 
                        }
                        if ($reposted==0) {
                            echo $this->Html->link(
                                'Repost',
                                array('controller' => 'reposts','action' => 'repost_allpost', $allposts['Post']['id'])
                            );
                        }
                    ?> &nbsp;

                    <!-- Count comments -->
                    <?php echo count($allposts['Comments']); ?>

                    <!-- Comments -->
                    <?php
                        echo $this->Html->link(
                            'Comments',
                            array('controller' => 'comments','action' => 'view', $allposts['Post']['id'])
                        );
                    ?>
                </div>

                <div class="edit-delete">
                    <?php
                        echo $this->Html->link(
                            'Edit',
                            array('action' => 'edit', $allposts['Post']['id'])
                        );
                    ?>
                    <?php
                        echo $this->Form->postLink(
                            'Delete',
                            array('action' => 'delete', $allposts['Post']['id']),
                            array('confirm' => 'Are you sure?')
                        );
                    ?>
                </div>
            </div>
            
        </div><br/>
    <?php endforeach; ?>
    <?php endif; ?>
    <?php unset($allposts); ?>

</div>

<style type="text/css">
    .like-repost {
        position: relative;
        left: 110px;
        top: -37px;
        width: 300px;
    }
</style>