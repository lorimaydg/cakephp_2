
<?php 
    $this->requestAction(array('controller'=>'posts','action'=>'redirect_check_edit',$this->params['pass'][0])); 
    $check_edit = $this->Session->read('check_edit');
    //pr($check_edit); 
?>

<?php if ($check_edit['User']['id']==$_SESSION['id']) : ?>

<?php echo $this->Html->link(
    'Back',
    array('controller' => 'posts', 'action' => 'index')
    ); 
?>

<br/><br/>
<h1>Edit Post</h1>

<?php
    echo $this->Form->create('Post');
    echo $this->Form->input('body', array('rows' => '3'));
    echo $this->Form->input('id', array('type' => 'hidden'));
    echo $this->Form->end('Save Post');
?>

<?php unset($_SESSION['post_id']); $_SESSION['post_id'] = $check_edit['Post']['id']; ?>

<?php 
    else :
        $this->requestAction(array('controller'=>'posts', 'action'=>'redirect_edit', $_SESSION['post_id']));
    endif;
?>