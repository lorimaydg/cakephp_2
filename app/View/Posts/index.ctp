
<?php echo $this->Html->css('home'); ?>
<?php $id_logged = $_SESSION['id']; ?>
<?php $with_posts = 0; ?>

<div class="container-home">

    <br/>
    <div class="header-menu">
        
        <?php echo $this->Html->link(
            'Home',
            array('controller' => 'posts', 'action' => 'index')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Notifications',
            array('controller' => 'follows', 'action' => 'follow_notification')
            ); 
        ?> <br/><br/>

        <?php echo $this->Html->link(
            'Add Post',
            array('controller' => 'posts', 'action' => 'add')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Search',
            array('controller' => 'users', 'action' => 'search')
            ); 
        ?> 
    
    </div>

    <br/><br/>       

    <?php //pr($posts); ?>
    <?php $your_reposts = array (); ?>
    
    <?php
        $this->requestAction(array(
            'controller' => 'follows',
            'action' => 'following'
        ));
    ?>

    <?php $followingz = $this->Session->read('followingz'); ?>
    <?php //pr($followingz); ?>

    <?php
        $followingz_array = array();
        foreach ($followingz as $followingzz) {
            array_push ($followingz_array, $followingzz['Following_User']['id']);
        }
    ?>
    <?php //pr($followingz_array); ?>

    <?php foreach ($posts as $post): ?>

        <!-- You reposted -->
        <?php
            $if_reposted = $post['Repost'];
            foreach ($if_reposted as $reposted) :
                if ($reposted['user_id']==$id_logged) :
                    array_push($your_reposts, $reposted['post_id']);
                endif;
            endforeach;
        ?>
        
        <?php if (in_array($post['User']['id'], $followingz_array) || $post['User']['id']==$id_logged) : ?>
        <?php if (!in_array($post['Post']['id'], $your_reposts)) : ?>

            <!-- Display Posts -->
            <div class="post-box">

                <div class="image">
                    <?php echo $this->Html->image('users/'.$post['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
                </div>

                <div class="user">
                    <b>
                        <?php 
                        if ($post['Post']['user_id']!=$id_logged) : 
                            echo $this->Html->link(
                                $post['User']['username'],
                                array('controller' => 'users','action' => 'view', $post['User']['id'])
                            ); 
                        else : 
                            echo h($post['User']['username']);
                        endif; 
                        ?>
                    </b>
                </div>

                <div class="date-created">
                    <?php
                        $m = array('January','February','March','April','May','June','July','August','September','October','November','December');
                        $date = explode ("-", $post['Post']['created']);
                        $no_time = explode (" ", $date[2]);
                        $day = $no_time[0];
                        $time = explode(":", $no_time[1]);
                        $hr = $time[0];
                        $min = $time[1];
                        echo h($m[$date[1]-1])." ".h($day).", ".h($date[0])." ".h($hr).":".h($min);
                    ?>
                </div>

                <div class="body">
                    <?php echo h($post['Post']['body']); ?>
                </div>

                <div>
                    <div class="like-repost">

                        <!-- Count likes -->
                        <?php
                            foreach ($likes as $like) {
                                if ($like['Post']['id']==$post['Post']['id']) {
                                    echo $this->Html->link(
                                        count($like['Like']),
                                        array('controller' => 'likes','action' => 'likers', $post['Post']['id'])
                                    );
                                }
                            }
                        ?>

                        <!-- Like or Unlike display -->
                        <?php
                            $liked = 0;
                            foreach ($likers as $liker) {
                                $likers2 = $liker['Like'];
                                foreach ($likers2 as $liker2) {
                                    if ($post['Post']['id']==$liker2['post_id'] && $logged['id']==$liker2['user_id']) {
                                        echo $this->Html->link(
                                            'Unlike',
                                            array('controller' => 'likes','action' => 'like', $post['Post']['id'])
                                        );
                                        $liked = 1; 
                                    } 
                                }
                            }
                            if ($liked==0) {
                                echo $this->Html->link(
                                    'Like',
                                    array('controller' => 'likes','action' => 'like', $post['Post']['id'])
                                );
                            }
                        ?> &nbsp;

                        <!-- Count reposts -->
                        <?php
                            foreach ($reposts as $repost) {
                                if ($repost['Post']['id']==$post['Post']['id']) {
                                    echo $this->Html->link(
                                        count($repost['Repost']),
                                        array('controller' => 'reposts','action' => 'reposters', $post['Post']['id'])
                                    );
                                }
                            }
                        ?>

                        <!-- Repost or Unrepost display -->
                        <?php
                            $reposted = 0;
                            foreach ($reposters as $reposter) {
                                $reposters2 = $reposter['Repost'];
                                foreach ($reposters2 as $reposter2) {
                                    if ($post['Post']['id']==$reposter2['post_id'] && $logged['id']==$reposter2['user_id']) {
                                        echo $this->Html->link(
                                            'Unrepost',
                                            array('controller' => 'reposts','action' => 'repost', $post['Post']['id'])
                                        );
                                        $reposted = 1; 
                                    } 
                                }
                            }
                            if ($reposted==0) {
                                echo $this->Html->link(
                                    'Repost',
                                    array('controller' => 'reposts','action' => 'repost', $post['Post']['id'])
                                );
                            }
                        ?> &nbsp;

                        <!-- Count comments -->
                        <?php
                            foreach ($comments as $comment) {
                                if ($comment['Post']['id']==$post['Post']['id']) {
                                    echo count($comment['Comments']);
                                }
                            }
                        ?>

                        <!-- Comments -->
                        <?php
                            echo $this->Html->link(
                                'Comments',
                                array('controller' => 'comments','action' => 'view', $post['User']['id'], $post['Post']['id']) 
                            );
                        ?>
                    </div>
                    <div class="edit-delete">
                        <?php if ($post['Post']['user_id']==$id_logged) : ?>
                        <?php
                            echo $this->Html->link(
                                'Edit',
                                array('action' => 'edit', $post['Post']['id'])
                            );
                        ?>
                        <?php
                            echo $this->Form->postLink(
                                'Delete',
                                array('action' => 'delete', $post['Post']['id']),
                                array('confirm' => 'Are you sure?')
                            );
                        ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div><br/>
            <?php $with_posts = 1; ?>

        <?php else : ?>

            <!-- Display Reposts -->
            <div class="post-box">

            <div class="image">
                <?php echo $this->Html->image('users/'.$post['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
            </div>

            <div class="user">
                <?php if ($post['Post']['user_id']==$id_logged) : ?>
                <b>You</b> reposted your own post
                <?php else: ?>
                <b>You</b> reposted <b>
                    <?php 
                        echo $this->Html->link(
                            $post['User']['username'],
                            array('controller'=>'users', 'action' => 'view', $post['User']['id'])
                        );
                    ?></b>'s post
            <?php endif; ?>
            </div>

            <div class="date-created">
                <?php
                    $m = array('January','February','March','April','May','June','July','August','September','October','November','December');
                    $date = explode ("-", $post['Post']['created']);
                    $no_time = explode (" ", $date[2]);
                    $day = $no_time[0];
                    $time = explode(":", $no_time[1]);
                    $hr = $time[0];
                    $min = $time[1];
                    echo h($m[$date[1]-1])." ".h($day).", ".h($date[0])." ".h($hr).":".h($min);
                ?>
            </div>

            <div class="body">
                <?php echo h($post['Post']['body']); ?>
            </div>

            <div>
                <div class="like-repost">

                        <!-- Count likes -->
                        <?php
                            foreach ($likes as $like) {
                                if ($like['Post']['id']==$post['Post']['id']) {
                                    echo $this->Html->link(
                                        count($like['Like']),
                                        array('controller' => 'likes','action' => 'likers', $post['Post']['id'])
                                    );
                                }
                            }
                        ?>

                        <!-- Like or Unlike display -->
                        <?php
                            $liked = 0;
                            foreach ($likers as $liker) {
                                $likers2 = $liker['Like'];
                                foreach ($likers2 as $liker2) {
                                    if ($post['Post']['id']==$liker2['post_id'] && $logged['id']==$liker2['user_id']) {
                                        echo $this->Html->link(
                                            'Unlike',
                                            array('controller' => 'likes','action' => 'like', $post['Post']['id'])
                                        );
                                        $liked = 1; 
                                    } 
                                }
                            }
                            if ($liked==0) {
                                echo $this->Html->link(
                                    'Like',
                                    array('controller' => 'likes','action' => 'like', $post['Post']['id'])
                                );
                            }
                        ?> &nbsp;

                        <!-- Count reposts -->
                        <?php
                            foreach ($reposts as $repost) {
                                if ($repost['Post']['id']==$post['Post']['id']) {
                                    echo $this->Html->link(
                                        count($repost['Repost']),
                                        array('controller' => 'reposts','action' => 'reposters', $post['Post']['id'])
                                    );
                                }
                            }
                        ?>

                        <!-- Repost or Unrepost display -->
                        <?php
                            $reposted = 0;
                            foreach ($reposters as $reposter) {
                                $reposters2 = $reposter['Repost'];
                                foreach ($reposters2 as $reposter2) {
                                    if ($post['Post']['id']==$reposter2['post_id'] && $logged['id']==$reposter2['user_id']) {
                                        echo $this->Html->link(
                                            'Unrepost',
                                            array('controller' => 'reposts','action' => 'repost', $post['Post']['id'])
                                        );
                                        $reposted = 1; 
                                    } 
                                }
                            }
                            if ($reposted==0) {
                                echo $this->Html->link(
                                    'Repost',
                                    array('controller' => 'reposts','action' => 'repost', $post['Post']['id'])
                                );
                            }
                        ?> &nbsp;

                        <!-- Count comments -->
                        <?php
                            foreach ($comments as $comment) {
                                if ($comment['Post']['id']==$post['Post']['id']) {
                                    echo count($comment['Comments']);
                                }
                            }
                        ?>

                        <!-- Comments -->
                        <?php
                            echo $this->Html->link(
                                'Comments',
                                array('controller' => 'comments','action' => 'view', $post['User']['id'], $post['Post']['id'])
                            );
                        ?>

                    </div>
                    <div class="edit-delete">
                        <?php if ($post['Post']['user_id']==$id_logged) : ?>
                        <?php
                            echo $this->Html->link(
                                'Edit',
                                array('action' => 'edit', $post['Post']['id'])
                            );
                        ?>
                        <?php
                            echo $this->Form->postLink(
                                'Delete',
                                array('action' => 'delete', $post['Post']['id']),
                                array('confirm' => 'Are you sure?')
                            );
                        ?>
                        <?php endif; ?>
                </div>
            </div>
            </div><br/>
            <?php $with_posts = 1; ?>

        <?php endif; ?>
        <?php endif; ?>

    <?php endforeach; ?>

    <br/>
    <?php if ($with_posts==1) : ?>
        <div class='pagination'>
            <?php echo $this->Paginator->prev('<<'); ?>
            <?php echo $this->Paginator->counter('Page {:page} of {:pages}'); ?>
            <?php echo $this->Paginator->next('>>'); ?><br/><br/><br/>
        </div>
    <?php else : ?>
        <h3 style="width: 100px; position: relative; left: 20px">No Posts</h3>
    <?php endif; ?>     

    <?php unset($post); ?>
    <?php unset($followingz); ?>
    
</div>
 <!-- <div class="suggestion">
    <?php //$this->fetch('suggestion'); ?>
</div>  -->

<style>

    .like-repost {
        position: relative;
        left: 110px;
        top: -37px;
        width: 300px;
    }

    .container-home {
        width: 670px;
       /* background: gray;*/
    }

    .suggestion {
        width: 200px;
        height: 200px;
        background: black;
        float: right;
        position: relative;
        /*margin: none;
        width: 260px;
        height: 550px;
        float: right;
        overflow: auto;
        background: lightgray;
        position: relative;
        left: 15px;
        top: -520px;*/
        /*left: 1000px;*/
        /*top: -525px;*/
        /*float: right;
        position: fixed;*/
    }

</style>