
<?php echo $this->Html->css(array('notifications')); ?>
<?php $id_logged = $_SESSION['id']; ?>

<div class="container-home">

    <br/>
    <div class="header-menu">

        <?php echo $this->Html->link(
            'Home',
            array('controller' => 'posts', 'action' => 'index')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Notifications',
            array('controller' => 'follows', 'action' => 'follow_notification')
            ); 
        ?> 
    
    </div>

    <div class="header-sub-menu">
        
        <?php echo $this->Html->link(
            'Follows',
            array('controller' => 'follows', 'action' => 'follow_notification')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Likes',
            array('controller' => 'likes', 'action' => 'likes_notification')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Reposts',
            array('controller' => 'reposts', 'action' => 'reposts_notification')
            ); 
        ?> 

    </div>

    <br/><br/>

    <?php if ($repost_notifs==null) : echo "<label style='position:relative;left:30px;width:300px'>No Repost Notifications</label>"; ?>
    <?php else : ?>
    <?php foreach ($repost_notifs as $repost_notif) : ?>

        <?php if ($repost_notif['User']['id']!=$id_logged) : ?>
        <div class="notifications">
            <div class="image">
                <?php echo $this->Html->image('users/'.$repost_notif['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
            </div>

            <div class="user">
                <b><?php echo h($repost_notif['User']['username']); ?></b> reposted your post
            </div>

            <div class="date-created">
                <?php echo h($repost_notif['Repost']['created']); ?>
            </div>

            <div class="body">
                "<?php echo h($repost_notif['Post']['body']); ?>"
            </div>
            <div class="view-profile">
                <?php
                    echo $this->Html->link(
                        'View Post',
                        array('controller' => 'posts', 'action' => 'view', $repost_notif['Post']['id'], $repost_notif['User']['id'])
                    );
                ?>
            </div>
        </div><br/>
        <?php endif; ?>

    <?php endforeach ?>
    <?php endif; ?>
    <?php unset($repost_notif); ?>

</div>
