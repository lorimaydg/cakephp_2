
<?php //$user_query = $this->Session->read('user_query'); ?>

<div class="sidebar">
    
    <br/>
    <div style="position: relative; left: 73px; top: 15px;">
        <?php echo $this->Html->image('users/'.$_SESSION['image'], array('height' => '150px', 'width' => '150px')); ?>
    </div>

    <br/><br/>
    <div class="name-sidebar" align="center">
        <label><b><?php echo h($_SESSION['firstname'])." ".h($_SESSION['lastname']) ?></b></label>
    </div>

    <div class="username-sidebar" align="center">
	    <label><?php echo h($_SESSION['username']) ?></label>
    </div>

    <br/>
    <div class="post-followers-following">
        <div class="post" align="center">
	        <?php echo $this->Html->link(
		            $_SESSION['countposts'],
		            array('controller' => 'posts', 'action' => 'allpost')
		        );
		    ?>
	    	<label class="header-label">POSTS</label>
	    </div>
	   	<div class="followers" align="center">
	   		<?php echo $this->Html->link(
		            $_SESSION['countFollowers'],
		            array('controller' => 'follows', 'action' => 'followers')
		        );
		    ?>
	   	    <label class="header-label">FOLLOWERS</label>
	   	</div>
    	<div class="following" align="center">
    	    <?php echo $this->Html->link(
		            $_SESSION['countFollowings'],
		            array('controller' => 'follows', 'action' => 'following')
		        );
		    ?>
	        <label class="header-label">FOLLOWING</label>
	   	</div>
    </div>

    <br/><br/><br/><br/><hr><br/>
    <div align="center">
    	<label>Details:</label><br/>
    	<p>
    	    Email: <?php echo h($_SESSION['email']); ?> <br/>
    	    Birthday: 
    	    <?php 
    	        $m = array('January','February','March','April','May','June','July','August','September','October','November','December');
    	        $bday = explode ("-", $_SESSION['birthday']);
    	        echo h($m[$bday[1]-1])." ".h($bday[2]).", ".h($bday[0])."<br/>";
    	    ?>
  	    </p> 

  	    <br/>
  	    <?php echo $this->Html->link(
	            'Edit Profile',
	            array('controller' => 'users', 'action' => 'edit', $_SESSION['id'])
	        ); 
	    ?> | 

	    <?php echo $this->Html->link(
	            'Logout',
	            array('controller' => 'users', 'action' => 'logout')
	        );
	    ?>
    </div>

</div>


<style type="text/css">

	.post {
        float: left;
		width: 95px;
	}

	.followers {
		float: left;
		width: 95px;
	}

	.following {
		float: left;
		width: 95px;
	}

	.header-label {
		font-size: 14px;
	}

</style>