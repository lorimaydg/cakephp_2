
<br/>
<?php $this->requestAction(array(
    'controller' => 'follows',
    'action' => 'followers'
)); ?>

<?php $followingz = $this->Session->read('followingz'); ?>

<h5>Suggestions:</h5><br/>

<?php foreach ($followingz as $following) : ?>
    <div class="suggestion-panel">
    	<div class="picture_suggestion">
    		<?php echo $this->Html->image('users/'.$following['Following_User']['image'], array('height' => '50px', 'width' => '50px')); ?>
    	</div>
        <b class="name_suggestion"><?php echo $following['Following_User']['firstname']." ".$following['Following_User']['lastname'] ?></b><br/>
        <text class="userrr_suggestion"><?php echo $following['Following_User']['username'] ?></text> <br/>
        <text class="view_suggestion">
        	<?php
                echo $this->Html->link(
                    'View Profile',
                    array('controller' => 'users', 'action' => 'view', $following['Following_User']['id'])
                );
            ?>
        </text>
    </div><br/>
<?php endforeach; ?>
<br/>

<style type="text/css">
	
	.suggestion-panel {
		border: 2px solid white;
		width: 220px;
		height: 80px;
		position: relative;
		left: 18px;
	}

	.picture_suggestion {
	    padding: 10px;
	}

	.name_suggestion {
		margin-left: -10px;
		position: relative;
		top: -70px;
		left: 100px;
		width: 150px;
		font-size: 12px;
	}

	.userrr_suggestion {
	    position: relative;
	    left: 90px;
	    top: -70px;
		font-size: 12px;
	}

	.view_suggestion {
		position: relative;
	    left: 90px;
	    top: -65px;
		font-size: 12px;
	}

	h5 {
		position: relative;
		left: 15px;
	}

</style>