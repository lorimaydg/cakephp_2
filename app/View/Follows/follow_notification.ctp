
<?php echo $this->Html->css(array('notifications')); ?>

<div class="container-home">

    <br/>
    <div class="header-menu">

        <?php echo $this->Html->link(
            'Home',
            array('controller' => 'posts', 'action' => 'index')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Notifications',
            array('controller' => 'follows', 'action' => 'follow_notification')
            ); 
        ?> 
    
    </div>

    <div class="header-sub-menu">
        
        <?php echo $this->Html->link(
            'Follows',
            array('controller' => 'follows', 'action' => 'follow_notification')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Likes',
            array('controller' => 'likes', 'action' => 'likes_notification')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Reposts',
            array('controller' => 'reposts', 'action' => 'reposts_notification')
            ); 
        ?> 

    </div>

    <br/><br/>
    
    <?php if ($follow_notifs==null) : echo "<label style='position:relative;left:30px;width:300px'>No Following Notifications</label>"; ?>
    <?php else : ?>
    <?php foreach ($follow_notifs as $follow_notif) : ?>

        <div class="notifications">
            <div class="image">
                <?php echo $this->Html->image('users/'.$follow_notif['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
            </div>

            <div class="user">
                <b><?php echo h($follow_notif['User']['username']); ?></b> followed you
            </div>

            <div class="date-created">
                <?php echo h($follow_notif['Follow']['created']); ?>
            </div>
            <div class="view-profile">
                <?php
                    echo $this->Html->link(
                        'View Profile',
                        array('controller' => 'users', 'action' => 'view', $follow_notif['User']['id'])
                    );
                ?>
            </div>
        </div><br/>
    <?php endforeach ?>
    <?php endif; ?>
    <?php unset($follow_notif); ?>

</div>
 