
<?php echo $this->Html->css(array('home','search')); ?>
<?php $id_logged = $_SESSION['id']; ?>

<div class="container-home">

    <br/>
    <div class="header-menu">

        <?php echo $this->Html->link(
            'Home',
            array('controller' => 'posts', 'action' => 'index')
            ); 
        ?> | 

        <?php echo $this->Html->link(
            'Notifications',
            array('controller' => 'follows', 'action' => 'follow_notification')
            ); 
        ?> 
    </div>

    <br/><br/>
    <h3 class="label">Who you follow:</h3><br/>

    <?php if ($followings==null) : echo "<label style='position:relative;left:30px;width:200px'>No Followings</label>"; ?>
    <?php else : ?>
    <?php foreach ($followings as $following): ?>

    <br/>
    <div class="result-box">

        <div class="image">
            <?php echo $this->Html->image('users/'.$following['Following_User']['image'], array('height' => '70px', 'width' => '70px')); ?>
        </div>
        <div class="name">
            <?php echo h($following['Following_User']['firstname'])." ".h($following['Following_User']['lastname']); ?>
        </div>
        <div class="username">
            <?php echo h($following['Following_User']['username']) ?>
        </div> 
        <div class="view-profile">
            <?php
                echo $this->Html->link(
                    'View Profile',
                    array('controller' => 'users', 'action' => 'view', $following['Following_User']['id'])
                );
            ?>
        </div>
    </div>

    <?php endforeach; ?>
    <?php endif; ?>
    <?php unset($following); ?>

</div>
