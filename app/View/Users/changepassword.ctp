
<br/><br/>
<h1>Change Password</h1>

<?php
    echo $this->Form->create('User');
    echo $this->Form->input('current_password', array('label'=>'Current Password', 'type'=>'password'));
    echo $this->Form->input('new_password', array('label'=>'New Password', 'type'=>'password')); 
    echo $this->Form->input('retype_new_password', array('label'=>'Retype New Password', 'type'=>'password'));

    echo $this->Form->end('Save Changes'); 
?>

<br/>
<?php echo $this->Html->link(
    'Back',
    array('controller' => 'users', 'action' => 'edit', $this->params['pass'][0])
    ); 
?>
