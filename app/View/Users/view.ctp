<!-- File: /app/View/Users/view.ctp -->

<?php echo $this->Html->css(array('view', 'home')); ?>
<?php $id_logged = $_SESSION['id'] ?>

<br/><br/>

<div class="container-view">

	<div class="left">
		<div class="image-view-user">
            <?php echo $this->Html->image('users/'.$user['User']['image'], array('height' => '120px', 'width' => '120px')); ?>
	    </div>
	</div>
    
    <div class="middle">
    	<div class="name" align="center">
		    <label class="name-label"><?php echo h($user['User']['firstname'])." ".h($user['User']['lastname']); ?></label>
		    <label>
                <label class="uname"><?php echo h($user['User']['username']); ?>
                    <label class="follows-you">
                        <?php
                            if ($_SESSION['follows_you']==1) {
                                echo "&nbsp;FOLLOWS YOU";
                            }
                        ?>
                    </label>
                </label>
            </label>
	    </div>
	    <div class="post-followers-following-view">
	    	<div class="post-view" align="center">
	    		<?php echo $_SESSION['view_countposts']; ?>
	    	    <label class="header-label">POSTS</label>
	    	</div>
	    	<div class="followers-view" align="center">
	    		<?php echo $_SESSION['view_countFollowers']; ?>
	    	    <label class="header-label">FOLLOWERS</label>
	    	</div>    
	    	<div class="following-view" align="center">
	    		<?php echo $_SESSION['view_countFollowings']; ?>
	    	    <label class="header-label">FOLLOWING</label>
	    	</div>
	    </div>
    </div>

    <div class="right" align="center">
    	<label class="details">Details: </label><br/>
    	<p>
    	    <?php echo "Email: ".h($user['User']['email']); ?><br/>
    	    <?php echo "Birthday: ".h($user['User']['birthday']); ?><br/>
    	    <?php echo "Joined: ".h($user['User']['created']); ?>
  	    </p> 

        <?php
            if ($_SESSION['you_followed']==1) {
      	   	    echo $this->Html->link(
                    'Unfollow',
                    array('controller' => 'follows', 'action' => 'follow', $user['User']['id'])
                );
            } else {
                echo $this->Html->link(
                    'Follow',
                    array('controller' => 'follows', 'action' => 'follow', $user['User']['id'])
                );
            }
        ?>
    </div>
    
</div>

<div class="post-container">

    <br/>
    <?php 
        echo $this->Html->link(
            'Back',
            array('controller' => 'posts', 'action' => 'index')
        );
    ?>

    <br/><br/>
	<?php //pr($posts); ?>
	<?php foreach ($posts as $post): ?>

    <?php if($post['Post']['id']!=null) : ?>
    <div class="post-box">
        
        <div class="image">
            <?php echo $this->Html->image('users/'.$post['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
        </div>

        <div class="user">
            <b><?php echo h($post['User']['username']); ?></b>
        </div>

        <div class="date-created">
            <?php echo h($post['Post']['created']); ?>
        </div>

        <div class="body">
            <?php echo h($post['Post']['body']); ?>
        </div>

        <div class="like-repost">
            <?php 
                echo $this->Html->link(
                    'View Post',
                    array('controller' => 'posts', 'action' => 'view', $post['Post']['id'], $post['User']['id'])
                );
            ?>
        </div>
	</div><br/>
    <?php else : echo "<h3 style='position:relative;left:15px'>No Posts</h3>"; endif; ?>

	<?php endforeach; ?>
	<?php unset($post); ?>
</div>

<style type="text/css">
    
    .container-view {
        margin-top: -40px;
        width: 980px;
        height: 160px;
        background: lightgray;
    }

    .like-repost {
        position: relative;
        left: 110px;
        top: -37px;
        width: 300px;
    }

    .follows-you {
        font-size: 10px;
        background: gray;
        width:80px;
        color: white;
    }

    .uname {
        width: 300px;
    }

</style>