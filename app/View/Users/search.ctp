
<?php echo $this->Html->link(
    'Back',
    array('controller' => 'posts', 'action' => 'index')
    ); 
?> 

<br/><br/><br/>

<?php echo $this->Form->create('User', array('type' => 'get', 'url' => 'search_result')); ?>
<fieldset>
    <legend><?php echo __('Search'); ?></legend>
    <?php
        echo $this->Form->input('search', array('name'=>'keyword'));
        echo $this->Form->submit('Search');
    ?>
</fieldset>

