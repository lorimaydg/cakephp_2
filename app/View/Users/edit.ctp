
<?php if ($_SESSION['id']==$this->params['pass'][0]) : ?>

<h1>Edit Profile</h1>

<?php
    echo $this->Form->create('User', array('enctype' => 'multipart/form-data'));
    echo $this->Form->input('firstname');
    echo $this->Form->input('lastname');
    echo $this->Form->input('username');
    echo $this->Form->input('email');
    echo $this->Form->input('birthday');
    echo $this->Form->input('id', array('type' => 'hidden'));

    echo $this->Form->input('image', array('type' => 'file'));

    echo $this->Form->end('Save Profile'); 

    $id = $this->request->params['pass'];

    echo $this->Html->link(
        'Cancel',
        array('controller' => 'posts', 'action' => 'index')
    ); 

    /*echo $this->Html->link(
        'Change Password',
        array ('action'=>'changepassword', $id[0])
    );*/
?>

<?php 
    else :
        $this->requestAction(array('controller'=>'users', 'action'=>'redirect_edit'));
    endif;
?>

