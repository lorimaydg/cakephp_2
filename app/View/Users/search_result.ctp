
<?php echo $this->Html->css('search'); ?>

<?php echo $this->Html->link(
    'Back',
    array('controller' => 'users', 'action' => 'search')
    ); 
?> 

<br/><br/><br/>

    <?php
        $keyword = $_GET['keyword'];
        echo "<h3>Search Results for: \"" . $keyword . "\"</h3>";
        $id_logged = $_SESSION['id'];
    ?>

    <br/>
    <?php if ($users==null) : echo "<label style='position:relative;left:30px;width:200px'>No Results</label>"; ?>
    <?php else : ?>

    <?php if($this->Paginator->counter('{:page}')>1) : 
        $searches = $_SESSION['add_searches']; 
        else : $searches = array();
    endif; ?>

    <?php //pr($searches); ?>
    <?php foreach ($users as $user): 
        if (!in_array($user['User']['id'], $searches)) :
        array_push ($searches, $user['User']['id']);
    ?>
    <?php if ($user['User']['id']!=$id_logged) : ?>
        <div class="result-box">
            <div class="image">
                <?php echo $this->Html->image('users/'.$user['User']['image'], array('height' => '70px', 'width' => '70px')); ?>
            </div>
            <div class="name">
                <?php echo h($user['User']['firstname'])." ".h($user['User']['lastname']); ?>
            </div>
            <div class="username">
                <?php echo h($user['User']['username']) ?>
            </div> 
            <div class="view-profile">
                <?php
                    echo $this->Html->link(
                        'View Profile',
                        array('action' => 'view', $user['User']['id'])
                    );
                ?>
            </div>
        </div>
    <?php endif; ?>
    <?php endif; ?>
    <?php endforeach; ?>
    <?php endif; ?>
    <br/>
    <div class='pagination'>
        <?php echo $this->Paginator->prev('<<'); ?>
        <?php echo $this->Paginator->counter('Page {:page} of {:pages}'); ?>
        <?php echo $this->Paginator->next('>>'); ?><br/><br/><br/>
    </div>
    <?php unset($_SESSION['add_searches']); $_SESSION['add_searches'] = $searches; ?>
    <?php //pr($searches); ?>
    <?php unset($user); ?>

<style type="text/css">
    
    .pagination {
        position: relative;
        left: 200px;
        width: 200px;
    }

</style>