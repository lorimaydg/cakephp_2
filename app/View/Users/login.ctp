
<?php $this->layout = 'login'; ?>

<br/><br/>

<div>
	<?php echo $this->Flash->render('auth'); ?>
	<?php echo $this->Form->create('User'); ?>
	    <fieldset>
	        <legend><?php echo __('Login'); ?></legend>
	        <?php 
	            echo $this->Form->input('username');
	            echo $this->Form->input('password');
	        ?>
	    </fieldset>
	<?php echo $this->Form->end(__('Login')); ?>

	<?php echo $this->Html->link(
	    'Sign up',
	    array('controller' => 'users', 'action' => 'add')
	    ); 
	?> | 
	<?php echo $this->Html->link(
	    'Forgot Password',
	    array('controller' => 'users', 'action' => 'forgot_password')
	    ); 
	?>
</div>

