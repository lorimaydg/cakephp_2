
<?php $this->layout = 'login'; ?>

<br/><br/>

<div>
	<?php echo $this->Flash->render('auth'); ?>
	<?php echo $this->Form->create('User'); ?>
	    <fieldset>
	        <legend><?php echo __('Forgot Password'); ?></legend>
	        <?php 
	            echo $this->Form->input('email_pass', array('label'=>'Email',));
	            echo $this->Form->input('new_password', array('type'=>'password'));
	            echo $this->Form->input('confirm_new_password', array('type'=>'password'));
	        ?>
	    </fieldset>
	<?php echo $this->Form->end(__('Send')); ?>

	<?php echo $this->Html->link(
	    'Cancel',
	    array('controller' => 'users', 'action' => 'login')
	    ); 
	?> 
</div>

