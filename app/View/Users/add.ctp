
<?php echo $this->Html->link(
    'Back',
    array('controller' => 'users', 'action' => 'login')
    ); 
?>

<?php $this->layout = 'login'; ?>

<br/><br/>
<div>
	<?php echo $this->Form->create('User'); ?>
	    <fieldset>
	        <legend><?php echo __('Sign Up'); ?></legend>
	        <?php 
	            echo $this->Form->input('firstname');
	            echo $this->Form->input('lastname');
	            echo $this->Form->input('username');
	            echo $this->Form->input('password');
	            echo $this->Form->input('email');
	            echo $this->Form->input('birthday', array(
				    'label' => 'Date of birth',
				    'dateFormat' => 'MDY',
				    'minYear' => date('Y') - 70,
				    'maxYear' => date('Y'),
				    'empty' => array(
				        'day' => 'DAY',
				        'month' => 'MONTH',
				        'year' => 'YEAR'
				    )
				));
	        ?>
	    </fieldset>
	<?php echo $this->Form->end(__('Sign Up')); ?>
</div>