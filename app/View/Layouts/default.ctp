<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">

		<div id="header">
			<h1>MicroBlog</h1>
			<?php echo $this->fetch('mainmenu'); ?>
		</div>
		
		<div id="content">
			<div class="sidebar">
				<?php echo $this->element('sidebar'); ?>
			</div>
			<div class="main">
		    	<?php echo $this->Flash->render(); ?>
			    <?php echo $this->fetch('content'); ?>
		    </div>
		 <!--    <div class="suggestion">
				<?php //echo $this->element('suggestion'); ?>
			</div> -->
		</div>
	</div>
</body>
</html>

<style type="text/css">
	
    #container {
        
    }

    #content {
    	height: 530px;
    	width: auto;
    	/*margin-left: 10px;*/
    }

    .sidebar {
    	margin: 5px;
    	width: 300px;
    	height: 550px;
    	float: left;
    	position: fixed;
    	background: lightgray;
    }

    .main {
    	/*width: 745px;*/
    	margin-top: 5px;
    	width: 1000px;
    	height: 520px;
    	overflow: auto;
    	position: relative;
    	left: 320px;
    	/*background: gray;*/
    }

   /* .suggestion {
    	margin: none;
    	overflow: auto;
    	width: 260px;
    	height: 550px;
    	float: right;
    	background: lightgray;
    	position: relative;
    	left: 15px;
    	top: -520px;
    	/*left: 1000px;*/
    	/*top: -525px;*/
    	/*float: right;
    	position: fixed;*/
    /*}*/

    h1 {
    	text-align: center;
    	font-size: 30px;
    }

    /* SCROLLER */

	/* width */
	::-webkit-scrollbar {
	    width: 10px;
	}

	/* Track */
	::-webkit-scrollbar-track {
	    background: #f1f1f1; 
	}
	 
	/* Handle */
	::-webkit-scrollbar-thumb {
	    background: #888; 
	    border-radius: 5px;
	}

	/* Handle on hover */
	::-webkit-scrollbar-thumb:hover {
	    background: #555; 
	}

</style>