
<?php

    App::uses('AppModel', 'Model');

    class Comment extends AppModel {
         
        public $actsAs = array('Containable');
        public $belongsTo = array('Post','User');

    	public $validate = array(
            'body' => array(
            	'bodyRule-1' => array (
                    'rule' => 'notBlank',
                    'message' => 'Post must not be empty'
            	),
                'bodyRule-2' => array (
                    'rule' => array ('maxLength',140),
                    'message' => 'Post is limited to 140 characters only'
                )
            )
        );
    }

?>