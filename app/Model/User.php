
<?php

    App::uses('AppModel', 'Model');
    App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
    App::uses('CakeEmail', 'Network/Email');

    class User extends AppModel {

        //public $useTable = true;
        public $actsAs = array ('Containable');
        //public $hasMany = array('Post', 'Follow');

        public $hasOne = array ('Post');
        public $hasMany = array ('Like','Repost','Comment');

        public $validate = array (

            'firstname' => array (
                'firstnameRule-1' => array (
                    'rule' => 'notBlank',
                    'message' => 'Firstname is required'
                ),
                'firstnameRule-2' => array (
                    'rule' => array ('custom', '/^[a-zA-Z ]*$/i'),
                    'message' => 'Name must not contain numbers or symbols'
                ),
                /*'firstnameRule-3' => array(
                    'rule' => array('custom', '/[\u3000-\u303F]|[\u3040-\u309F]|[\u30A0-\u30FF]|[\uFF00-\uFFEF]|[\u4E00-\u9FAF]|[\u2605-\u2606]|[\u2190-\u2195]|\u203B/g'),
                    'message' => 'Japanese Characters'
                )*/
            ),

            'lastname' => array (
                'lastnameRule-1' => array(
                    'rule' => 'notBlank',
                    'message' => 'Lastname is required'
                ),
                'lastnameRule-2' => array(
                    'rule' => array ('custom', '/^[a-zA-Z ]*$/i'),
                    'message' => 'Name must not contain numbers or symbols'
                )
            ),

            'username' => array (
                'usernameRule-1' => array (
                    'rule' => 'notBlank',
                    'message' => 'Username is required'
                ),
                'usernameRule-2' => array (
                    'rule' => 'alphaNumeric',
                    'message' => 'Username must contain only letters and numbers'
                ),
                'usernameRule-3' => array (
                    'rule' => 'isUnique',
                    'message' => 'Username already in use'
                )
            ),

            'password' => array (
                'passwordRule-1' => array (
                    'rule' => 'notBlank',
                    'message' => 'Password is required'
                ),
                'passwordRule-2' => array (
                    'rule' => array ('minLength',8),
                    'message' => 'Password must be at least 8 characters long'
                ),
                'passwordRule-3' => array (
                    'rule' => 'alphaNumeric',
                    'message' => 'Password must be composed of alphanumeric characters'
                )
            ),

            'current_password' => array (
                'empty' => array (
                    'rule' => 'notBlank',
                    'message' => 'Password is required'
                ),
                'unmatch' => array (
                    'rule' => 'verify_password',
                    'message' => 'Invalid Password'
                )
            ),

            'new_password' => array (
                'empty' => array (
                    'rule' => 'notBlank',
                    'message' => 'Password is required'
                ),
                'length' => array (
                    'rule' => array ('minLength',8),
                    'message' => 'Password must be at least 8 characters long'
                ),
            ),

            'confirm_new_password' => array (
                'empty' => array (
                    'rule' => 'notBlank',
                    'message' => 'Password is required'
                ),
                'length' => array (
                    'rule' => array ('minLength',8),
                    'message' => 'Password must be at least 8 characters long'
                ),
                'compare' => array (
                    'rule' => array ('validate_passwords'),
                    'message' => 'The passwords you entered do not match.',
                )
            ),

            'email' => array (
                'emailRule-1' => array (
                    'rule' => 'notBlank',
                    'message' => 'Email Address is required'
                ),
                'emailRule-2' => array (
                    'rule' => array('email', true),
                    'message' => 'Invalid Email Address'
                ),
                'emailRule-3' => array (
                    'rule' => 'isUnique',
                    'message' => 'Email Address already in use'
                ),
            ),

            'email_pass' => array (
                'emailRule-1' => array (
                    'rule' => 'notBlank',
                    'message' => 'Email Address is required'
                ),
                'emailRule-2' => array (
                    'rule' => array('email', true),
                    'message' => 'Invalid Email Address'
                )
            ),

            'birthday' => array (
                'birthdayRule-1' => array (
                    'rule' => 'validateBirthday',
                    'message' => 'Please enter a valid date'
                ),
                'birthdayRule-2' => array (
                    'rule' => 'notBlank',
                    'message' => 'Birthday required'
                )
            )
        );

        public function validateBirthday($check) {

            $value = array_values($check);
            $value = $value[0];
            $now = date('Y-m-d');

            if ($now > $value) {
                return true;
            } else {
                return false;
            }
        }

        public function beforeSave($options = array ()) {    

            if (isset($_SESSION['forgot_password'])) {
                $passwordHasher = new BlowfishPasswordHasher();
                $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $_SESSION['forgot_password']
                );  
                return true;
            }

            if (isset($this->data[$this->alias]['password'])) {
                $passwordHasher = new BlowfishPasswordHasher();
                $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password']
                );
                return true;
            }
        }

    }

?>