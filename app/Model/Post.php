
<?php

    App::uses('AppModel', 'Model');

    class Post extends AppModel {

        public $actsAs = array('Containable');
        public $belongsTo = array('User');
        public $hasMany = array('Like','Repost','Comments');

        public $validate = array(
            'body' => array(
            	'bodyRule-1' => array (
                    'rule' => 'notBlank',
                    'message' => 'Post must not be empty'
            	),
                'bodyRule-2' => array (
                    'rule' => array ('maxLength',140),
                    'message' => 'Post is limited to 140 characters only'
                )
            )
        );
    }

?>