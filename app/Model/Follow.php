
<?php

    App::uses('AppModel', 'Model');

    class Follow extends AppModel {

        public $actsAs = array('Containable');
        public $belongsTo = array('User',
            'Following_User' => array(
	            'className' => 'User',
		        'foreignKey' => false,
		        'conditions' => array(
		        	'Following_User.id = Follow.follow_id'
		        )
	        )
	    );
    }

?>